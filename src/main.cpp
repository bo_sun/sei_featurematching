/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Jun 06, 2015                        *
 * Licensing: GNU GPL license.               *
 *********************************************/
#define PCL_NO_PRECOMPILE
#include <Eigen/Dense>
#include <pcl/correspondence.h>
#include <pcl/common/transforms.h>
#include <pcl/conversions.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/features/feature.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <iostream>
#include <fstream>

#include <ctime>
#include <chrono>
#include <ratio>

#include "tams_feature_type.hpp"
#include "tams_feature.h"
#include "tams_feature.hpp"
#include "tams_PhaseCorrSearch.h"
#include "tams_PhaseCorrSearch.hpp"

using namespace pcl;
using namespace tams;
using namespace std::chrono;

// Typedef Types
#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef tams::TAMSFeatureType FeatureT;
typedef pcl::PointCloud<FeatureT> FeatureCloudT;
#endif /*TYPE_DEFINITION_*/

std::string model_filename_;
std::string scene_filename_;
std::string model_keypoints_filename_;
std::string scene_keypoints_filename_;

bool  property(false);
bool  show_keypoints_(false);
bool  show_correspondences_(false);
bool  use_mr_ (true);
int   keypoints_num_(100);
float support_size_ (30);
float match_features_threshold_ (0.95f);

void
showHelp (char *filename)
{
  std::cout << std::endl;
  std::cout << "***************************************************************************" << std::endl;
  std::cout << "*                                                                         *" << std::endl;
  std::cout << "*             Feature Matching Experiments     - Usage Guide              *" << std::endl;
  std::cout << "*                                                                         *" << std::endl;
  std::cout << "***************************************************************************" << std::endl << std::endl;
  std::cout << "Usage: " << filename << " model_filename.pcd scene_filename.pcd" << std::endl;
  std::cout <<                          " model_keypoints_filename.pcd scene_keypoints_filename.pcd" << std::endl;
  std::cout <<                          "[Options]" << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "     -h:                     Show this help." << std::endl;
  std::cout << "     -property               Show the property of the point clouds." << std::endl;
  std::cout << "     -k:                     Show used keypoints." << std::endl;
  std::cout << "     -c:                     Show used correspondences." << std::endl;
  std::cout << "     -usr_mr:                Compute the model cloud resolution and multiply." << std::endl;
  std::cout << "     --keypoints_num:        Number of keypoints used (default 100)." << std::endl;
  std::cout << "     --support_size:         Descriptor size (default 0.01)." << std::endl;
  std::cout << "     --match_features_threshold:" << std::endl;
  std::cout << "            Threshold of the distances between correspondences." << std::endl;
}

void
parseCommandLine (int argc, char *argv[])
{
  //Show help
  if (pcl::console::find_switch (argc, argv, "-h"))
  {
    showHelp (argv[0]);
    exit (0);
  }

  //Model & scene filenames
  std::vector<int> filenames;
  filenames = pcl::console::parse_file_extension_argument (argc, argv, ".pcd");
  if (filenames.size () != 4)
  {
    std::cout << "Filenames missing.\n";
    showHelp (argv[0]);
    exit (-1);
  }

  model_filename_ = argv[filenames[0]];
  scene_filename_ = argv[filenames[1]];
  model_keypoints_filename_ = argv[filenames[2]];
  scene_keypoints_filename_ = argv[filenames[3]];

  //Program behavior
  if (pcl::console::find_switch(argc, argv, "-property"))
  {
     property = 1;
  }
  if (pcl::console::find_switch (argc, argv, "-k"))
  {
    show_keypoints_ = true;
  }
  if (pcl::console::find_switch (argc, argv, "-c"))
  {
    show_correspondences_ = true;
  }
  if (pcl::console::find_switch (argc, argv, "-use_mr"))
  {
    use_mr_ = true;
  }

  //General parameters
  pcl::console::parse_argument (argc, argv, "--keypoints_num", keypoints_num_);
  pcl::console::parse_argument (argc, argv, "--support_size", support_size_);
  pcl::console::parse_argument (argc, argv, "--match_features_threshold", match_features_threshold_);
}

double
computeCloudResolution (const PointCloudT::ConstPtr &cloud)
{
  double res = 0.0;
  int n_points = 0;
  int nres;
  std::vector<int> indices (2);
  std::vector<float> sqr_distances (2);
  pcl::search::KdTree<PointT> tree;
  tree.setInputCloud (cloud);

  for (size_t i = 0; i < cloud->size (); ++i)
  {
    if (! pcl_isfinite ((*cloud)[i].x))
    {
      continue;
    }
    //Considering the second neighbor since the first is the point itself.
    nres = tree.nearestKSearch (i, 2, indices, sqr_distances);
    if (nres == 2)
    {
      res += sqrt (sqr_distances[1]);
      ++n_points;
    }
  }
  if (n_points != 0)
  {
    res /= n_points;
  }
  return res;
}

int
main (int argc, char **argv)
{
    parseCommandLine(argc, argv);

    // Initiate Point clouds
    PointCloudT::Ptr model (new PointCloudT);
    PointCloudT::Ptr model_keypoints (new PointCloudT);
    FeatureCloudT::Ptr model_features (new FeatureCloudT);
    PointCloudT::Ptr scene (new PointCloudT);
    PointCloudT::Ptr scene_keypoints (new PointCloudT);
    FeatureCloudT::Ptr scene_features (new FeatureCloudT);

    // Load point clouds
    if (pcl::io::loadPCDFile (model_filename_, *model) < 0 ||
            pcl::io::loadPCDFile (scene_filename_, *scene) < 0)
    {
      std::cout << "Error loading model/scene cloud." << std::endl;
      showHelp (argv[0]);
      return (-1);
    }
    // Load keypoints
    if (pcl::io::loadPCDFile (model_keypoints_filename_, *model_keypoints) < 0 ||
            pcl::io::loadPCDFile (scene_keypoints_filename_, *scene_keypoints) < 0)
    {
      std::cout << "Error loading model/scene keypoints." << std::endl;
      showHelp (argv[0]);
      return (-1);
    }

    if(model_keypoints->size() != scene_keypoints->size())
    {
        pcl::console::print_error("The number of keypoints in model and scene is not equal!...\n");
        return (-1);
    }
    pcl::console::print_info("The number of valid correspondences is %i.\n", scene_keypoints->size());

    if (property)
    {
        double model_resolution = 0.0, scene_resolution = 0.0;
        model_resolution = computeCloudResolution(model);
        scene_resolution = computeCloudResolution(scene);

        pcl::console::print_info("The model has %d points without resample.\n", model->size());
        pcl::console::print_info("The scene  has %d points without resample.\n", scene->size());
        pcl::console::print_info("The resolution of model is %f without resample. \n", model_resolution);
        pcl::console::print_info("The resolution of scene  is %f without resample. \n", scene_resolution);

        PointT minpt, maxpt;
        pcl::getMinMax3D<PointT> (*model, minpt, maxpt);
        pcl::console::print_info("The range of model is [x: %f, y: %f, z: %f]\n",
                                 maxpt.x - minpt.x,
                                 maxpt.y - minpt.y,
                                 maxpt.z - minpt.z);
        pcl::getMinMax3D<PointT> (*scene, minpt, maxpt);
        pcl::console::print_info("The range of scene is [x: %f, y: %f, z: %f]\n",
                                 maxpt.x - minpt.x,
                                 maxpt.y - minpt.y,
                                 maxpt.z - minpt.z);
        return (0);
    }

    if(use_mr_)
    {
        float resolution = static_cast<float> (computeCloudResolution(model));
        if(resolution != 0.0f)
        {
            support_size_ *= resolution;
        }
        std::cout << "Model resolution:         " << resolution << std::endl;
        std::cout << "Descriptor support size:  " << support_size_ << std::endl;
    }


    // Compute Features for keypoints
    pcl::console::print_highlight ("Estimating features...\n");
    high_resolution_clock::time_point t1=high_resolution_clock::now();
    tams::TAMSFeatureEstimation<PointT, FeatureT> fest;
    fest.setInputCloud (model_keypoints);
    fest.setSearchSurface(model);
    fest.setRadiusSearch(support_size_);
    fest.computeFeature(*model_features);
    fest.setInputCloud(scene_keypoints);
    fest.setSearchSurface(scene);
    fest.computeFeature(*scene_features);
    high_resolution_clock::time_point t2=high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> > (t2-t1);
    pcl::console::print_info("Feature Computing uses %f seconds (Total).\n", time_span.count()/2);
    pcl::console::print_info("Feature Computing uses %f seconds (Total/corr).\n", 0.5*time_span.count()/model_features->size());
    pcl::console::print_info("Feature Computing uses %f seconds (Total/corr).\n", 0.5*time_span.count()/scene_features->size());
    pcl::io::savePCDFileASCII ("model_feature.pcd", *model_features);
    pcl::io::savePCDFileASCII("scene_feature.pcd", *scene_features);

    // Find Model-Scene Correspondences with KdTree
    pcl::console::print_highlight("Finding model-scene correspondences...\n");
    pcl::CorrespondencesPtr model_scene_corrs (new pcl::Correspondences());

    // For each scene features, find nearest neighbor into the model features,
    // and add it to correspondence vector.

    for (size_t i =0; i < scene_features->size() ; ++i)
    {
        if (!pcl_isfinite(scene_features->at(i).descriptor[0]))
            continue;

        int idx = -1;
        float dist = -10000.0;
        tams::SearchNearest(scene_features->at(i), model_features, idx , dist);
        if (idx >=0 && dist > match_features_threshold_)
        {
            pcl::Correspondence corr(idx, static_cast<int> (i), dist);
            model_scene_corrs->push_back(corr);
        }
    }

    pcl::console::print_highlight("Correspondences found: %i \n", model_scene_corrs->size());

    // compute recall and precision
    size_t relevant = model_keypoints->size();
    size_t retrival = model_scene_corrs->size();
    size_t retrived_relevant = 0;
    for(size_t i = 0; i < model_scene_corrs->size(); ++i)
    {
        if(model_scene_corrs->at(i).index_query == model_scene_corrs->at(i).index_match)
        {
            retrived_relevant++;
        }
    }

    pcl::console::print_highlight("======= Results =======\n");
    pcl::console::print_highlight("Threshold: %6.6f \n", match_features_threshold_);
    pcl::console::print_highlight("Retrived & Relevant: %i\n", retrived_relevant);
    pcl::console::print_highlight("Recall: %6.6f \n", float(retrived_relevant)/float(relevant));
    pcl::console::print_highlight("1-Precision: %6.6f \n", 1-float(retrived_relevant)/float(retrival));

    // save the correspondences into text file
    ofstream corrs_file;
    corrs_file.open("corres.txt");
    corrs_file << "Model Index" << " " << "Scene Index" << " " << "Squred Distance" << std::endl;
    for (size_t i=0; i<model_scene_corrs->size(); i++)
    {
        corrs_file << model_scene_corrs->at(i).index_query << "  "
                      << model_scene_corrs->at(i).index_match << "  "
                         << model_scene_corrs->at(i).distance << std::endl;
    }
    corrs_file.close();

    // Visualization
    pcl::visualization::PCLVisualizer viewer ("Correspondence");
    viewer.setBackgroundColor(255,255,255);
    pcl::visualization::PointCloudColorHandlerCustom<PointT> scene_color_handler (scene, 0,255,0);
    viewer.addPointCloud(scene, scene_color_handler, "scene_cloud");
    PointCloudT::Ptr off_scene_model (new PointCloudT);
    PointCloudT::Ptr off_scene_model_keypoints (new PointCloudT);
    if (show_correspondences_ || show_keypoints_)
    {
        pcl::transformPointCloud(*model, *off_scene_model,
                                 Eigen::Vector3f(-0.1, 0, 0), Eigen::Quaternionf(1,0,0,0));
        pcl::transformPointCloud(*model_keypoints, *off_scene_model_keypoints,
                                 Eigen::Vector3f(-0.1, 0, 0), Eigen::Quaternionf(1,0,0,0));
        pcl::visualization::PointCloudColorHandlerCustom<PointT> off_scene_model_color_handler (off_scene_model, 0,0,255);
        viewer.addPointCloud(off_scene_model, off_scene_model_color_handler, "off_scene_model");
    }

    if (show_keypoints_)
    {
        pcl::visualization::PointCloudColorHandlerCustom<PointT> scene_keypoints_color_handler (scene_keypoints, 255, 0, 0);
        viewer.addPointCloud (scene_keypoints, scene_keypoints_color_handler, "scene_keypoints");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "scene_keypoints");

        pcl::visualization::PointCloudColorHandlerCustom<PointT> off_scene_model_keypoints_color_handler (off_scene_model_keypoints, 255, 0, 0);
        viewer.addPointCloud (off_scene_model_keypoints, off_scene_model_keypoints_color_handler, "off_scene_model_keypoints");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "off_scene_model_keypoints");
    }

    if (show_correspondences_)
    {
        for (size_t j = 0; j < model_scene_corrs->size (); ++j)
        {
            if(model_scene_corrs->at(j).index_query != model_scene_corrs->at(j).index_match)
                continue;
            std::stringstream ss_line;
            ss_line << "correspondence_line" << j;
            PointT& model_point = off_scene_model_keypoints->at (model_scene_corrs->at(j).index_query);
            PointT& scene_point = scene_keypoints->at (model_scene_corrs->at(j). index_match);

            //  We are drawing a line for each pair of clustered correspondences found between the model and the scene
            viewer.addLine<PointT, PointT> (model_point, scene_point, 255, 0, 128, ss_line.str ());
        }
    }

    viewer.spin();

    return (0);
}

