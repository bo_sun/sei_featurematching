/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Jun 3, 2015                        *
 * Licensing: GNU GPL license.               *
 *********************************************/
#ifndef TAMS_SEARCH_IMPL_H_
#define TAMS_SEARCH_IMPL_H_

#include "tams_PhaseCorrSearch.h"

void tams::CompareVector(const float *signal,
                    const float *pattern,
                    const int size,
                    float &dist)
{
    fftw_complex *signal_vector = (fftw_complex*) fftw_malloc (sizeof(fftw_complex)*size);
    fftw_complex *pattern_vector = (fftw_complex*) fftw_malloc (sizeof(fftw_complex)*size);

    for (int i=0; i < size; i++)
    {
        signal_vector[i][0] = *(signal+i);
        signal_vector[i][1] = 0;
    }
    for (int j=0; j < size; j++)
    {
        pattern_vector[j][0] = *(pattern+j);
        pattern_vector[j][1] = 0;
    }

    // forward fft
    fftw_plan signal_forward_plan = fftw_plan_dft_1d(size, signal_vector, signal_vector,
                                                     FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pattern_forward_plan  = fftw_plan_dft_1d(size, pattern_vector, pattern_vector,
                                                     FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute (signal_forward_plan);
    fftw_execute (pattern_forward_plan);

    // cross power spectrum
    fftw_complex *cross_vector = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*size);
    double temp;
    for (int i=0; i<size; i++)
    {
        cross_vector[i][0] = (pattern_vector[i][0]*signal_vector[i][0])-
                (pattern_vector[i][1]*(-signal_vector[i][1]));
        cross_vector[i][1] = (pattern_vector[i][0]*(-signal_vector[i][1]))+
                (pattern_vector[i][1]*signal_vector[i][0]);
        temp = sqrt(cross_vector[i][0]*cross_vector[i][0]+cross_vector[i][1]*cross_vector[i][1]);
        cross_vector[i][0] /= temp;
        cross_vector[i][1] /= temp;
    }

    // backward fft
    // FFTW computes an unnormalized transform,
    // in that there is no coefficient in front of
    // the summation in the DFT.
    // In other words, applying the forward and then
    // the backward transform will multiply the input by n.

    // BUT, we only care about the maximum of the inverse DFT,
    // so we don't need to normalize the inverse result.

    // the storage order in FFTW is row-order
    fftw_plan cross_backward_plan = fftw_plan_dft_1d(size, cross_vector, cross_vector,
                                                     FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(cross_backward_plan);

    // free memory
    fftw_destroy_plan(signal_forward_plan);
    fftw_destroy_plan(pattern_forward_plan);
    fftw_free(signal_vector);
    fftw_free(pattern_vector);

    Eigen::VectorXf cross_real = Eigen::VectorXf::Zero(size);
    for (int i=0; i < size; i++)
    {
        cross_real(i) = cross_vector[i][0];
    }
    std::ptrdiff_t max_loc;
    dist = fabs(cross_real.maxCoeff(&max_loc)/size);
}

void tams::SearchNearest(const FeatureT &query, const FeatureCloudT::ConstPtr inputCloud,
                         int &idx , float &dist)
{
    float temp_dist = -100000.0;
    for(size_t i=0; i<inputCloud->size(); ++i)
    {
        tams::CompareVector(query.descriptor, inputCloud->at(i).descriptor, query.descriptorSize(),temp_dist);
        if(temp_dist>dist)
        {
            dist = temp_dist;
            idx = i;
        }
    }
}
#endif /*TAMS_SEARCH_HPP_*/
