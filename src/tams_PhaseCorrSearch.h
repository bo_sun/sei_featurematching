/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Jun 3, 2015                        *
 * Licensing: GNU GPL license.               *
 *********************************************/
#ifndef TAMS_SEARCH_H_
#define TAMS_SEARCH_H_

#include <fftw3.h>

// Typedef Types
#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef tams::TAMSFeatureType FeatureT;
typedef pcl::PointCloud<FeatureT> FeatureCloudT;
#endif /*TYPE_DEFINITION_*/

namespace tams{
void CompareVector(const float *signal,
                   const float *pattern,
                   const int size,
                   float &dist);

void SearchNearest(const FeatureT &query, const FeatureCloudT::ConstPtr inputCloud,
                   int &idx , float &dist);

}  /*end of namespace*/
#endif /*TAMS_SEARCH_H_*/
